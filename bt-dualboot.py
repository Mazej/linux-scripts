#!/usr/bin/env python
import os
import subprocess
import re

key = ""
host_mac = "9822efa1b154" #NOTE change this to actual mac of host device
windows_mount_point = "/mnt/tmp" #NOTE change this to actual windows mountpoint


def mac_addr_to_linux_format(mac):
    linux_mac = ""
    for i in range(0, len(mac), 2):
        linux_mac += mac[i:i+2] + ":"
    return linux_mac[:-1].upper()


def prepare_command(host_mac, key=None):
    command = f"""cd {windows_mount_point}/Windows/System32/config;chntpw -e SYSTEM <<-CommandsIndicatorString
    cd ControlSet001\Services\BTHPORT\Parameters\Keys\{host_mac}
    {('hex '+ key )if key else 'ls'}
    q
    CommandsIndicatorString
    )"""
    return command


def get_link_key(device_key):
    ret = subprocess.run(prepare_command(
        host_mac, device_key), capture_output=True, shell=True)
    link_key = re.search(
        "(([A-Z0-9]{2} ?){16})", ret.stdout.decode()).group(0).replace(" ", "")
    return link_key


def get_mac_link_key_from_chntpw(chtnpw_output):
    devices = {}
    header_found = False
    for line in chtnpw_output.splitlines():
        if header_found:
            if line.strip() == "":
                break

            device_key = re.search(".*<(.*)>.*", line).group(1)
            link_key = get_link_key(device_key)

            devices[device_key] = link_key

        if "size     type              value name             [value if type DWORD]" in line:
            header_found = True
    return devices


def correct_local_link_keys(macs_and_keys):
    linux_host_mac = mac_addr_to_linux_format(host_mac)
    for mac, link_key in macs_and_keys.items():
        linux_format = mac_addr_to_linux_format(mac)
        info_file = f"/var/lib/bluetooth/{linux_host_mac}/{linux_format}/info"
        if (not os.path.isfile(info_file)):
            continue
        with open(info_file, "r") as f:
            link_key_section = False
            corrected_lines = []
            for line in f.readlines():
                if "[LinkKey]" in line:
                    link_key_section = True
                elif link_key_section and "Key=" in line:
                    line = f"Key={link_key}\n"
                corrected_lines.append(line)

        with open(info_file, 'w') as f:
            f.writelines(corrected_lines)


def restart_bluetooth():
    ret = subprocess.call(["sudo", "systemctl", "restart", "bluetooth"])
    assert(ret == 0)


if not os.path.isdir(f"{windows_mount_point}/Windows/System32/config"):
    print("Error: Windows is not mounted or wrong mount point is set")
    exit(1)

chtnpw_output = subprocess.run(prepare_command(host_mac),
                               capture_output=True, shell=True).stdout.decode()

macs_and_keys = get_mac_link_key_from_chntpw(chtnpw_output)
correct_local_link_keys(macs_and_keys)
restart_bluetooth()
