#!/bin/bash
sudo apt-get update
sudo apt-get upgrade -y
mkdir Downloads

sudo apt-get install gparted git gnome-tweak-tool -y

# Spotify installation
curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add - 
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt-get update && sudo apt-get install spotify-client -y

# VS Code installation
cd Downloads
wget https://az764295.vo.msecnd.net/stable/d2e414d9e4239a252d1ab117bd7067f125afd80a/code_1.50.1-1602600906_amd64.deb
sudo dpkg -i code_*
# Install Setting sync
code --install-extension Shan.code-settings-sync


# Slack installation
cd Downloads
wget https://downloads.slack-edge.com/linux_releases/slack-desktop-4.10.3-amd64.deb
sudo dpkg -i slack-desktop-*

# Papirus icons
sudo add-apt-repository ppa:papirus/papirus
sudo apt-get update
sudo apt-get install papirus-icon-theme -y
gsettings set org.gnome.desktop.interface icon-theme 'Papirus'

# Orchis theme
sudo apt install gtk2-engines-murrine -y
cd Downloads
git clone https://github.com/vinceliuice/Orchis-theme.git
cd Orchis-theme
./install.sh
cd..
gsettings set org.gnome.desktop.interface gtk-theme "Orchis-light-compact"
gsettings set org.gnome.desktop.wm.preferences theme "Orchis-light-compact"
